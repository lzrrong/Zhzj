package com.zhzj;

import android.app.Application;

import com.tencent.sonic.sdk.SonicConfig;
import com.tencent.sonic.sdk.SonicEngine;
import com.zhzj.interfaces.SonicRuntimeImpl;

/**
 * Created by 10302 on 2018/12/20.
 */

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        if (!SonicEngine.isGetInstanceAllowed()) {
            SonicEngine.createInstance(new SonicRuntimeImpl(this), new SonicConfig.Builder().build());
        }
    }
}
