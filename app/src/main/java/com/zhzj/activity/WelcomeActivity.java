package com.zhzj.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.tbruyelle.rxpermissions2.RxPermissions;
import com.zhzj.R;

public class WelcomeActivity extends AppCompatActivity {

    private static Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        requestAppPermissions();
    }

    private void requestAppPermissions() {
        RxPermissions permissions = new RxPermissions(this);
        //同时请求多个权限
        permissions.request(Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe(new io.reactivex.functions.Consumer<Boolean>() {
                               @Override
                               public void accept(Boolean aBoolean) throws Exception {
                                   if (aBoolean) {
                                       //所有权限都开启aBoolean才为true，否则为false
//                            toastShort("已成功获取授权");
                                       doMyWorks();
                                   } else {
                                       toastShort("权限被拒绝，请在设置里面开启相应权限，若无相应权限会影响使用");
                                       doMyWorks();
                                   }
                               }
                           }
                );
    }

    private void doMyWorks() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(WelcomeActivity.this, MainActivity.class));
                finish();
            }
        }, 1000);
    }


    /**
     * 显示短toast
     *
     * @param msg
     */
    public void toastShort(String msg) {
        if (toast == null) {
            toast = Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT);
        }
        toast.setText(msg);
        toast.show();
    }

}
