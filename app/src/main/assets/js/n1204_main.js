  /**
 * 动态加载CSS
 * @param {string} url 样式地址
 */
function dynamicLoadCss(url) {
    var head = document.getElementsByTagName('head')[0];
    var link = document.createElement('link');
    link.type='text/css';
    link.rel = 'stylesheet';
    link.href = url;
    head.appendChild(link);
}
/**
 * 动态加载JS
 * @param {string} url 脚本地址
 * @param {function} callback  回调函数
*/
function dynamicLoadJs(url, callback) {
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;
    if(typeof(callback)=='function'){
        script.onload = script.onreadystatechange = function () {
            if (!this.readyState || this.readyState === "loaded" || this.readyState === "complete"){
                callback();
                script.onload = script.onreadystatechange = null;
            }
        };
    }
    head.appendChild(script);
}

// 加载css和js
(function(){
	// 根据屏幕宽度计算fontSize 适配屏幕大小
	! function(a, b) {
		var c = a.documentElement,
			d = "orientationchange" in window ? "orientationchange" : "resize",
			e = function() {
				var a = c.clientWidth;
				a && (c.style.cssText = 'font-size:' + 100 * (a / 750) + "px!important;")
			};
		a.addEventListener && (b.addEventListener(d, e, !1), a.addEventListener("DOMContentLoaded", e, !1))
	}(document, window);
	
	// api: http://weui.shanliwawa.top/
	dynamicLoadCss('./css/reset.css'); // 公用css
	// weui5.0 css丶js
	dynamicLoadCss('./js/weui5.0/css/icon.css');
	dynamicLoadCss('./js/weui5.0/css/weui.css');
	dynamicLoadCss('./js/weui5.0/css/weuix.css'); 
	dynamicLoadJs('./js/weui5.0/js/zepto.min.js');
    dynamicLoadJs('./js/weui5.0/js/zepto.weui.js'); 
    
    // layer 手机版
    dynamicLoadCss('./js/layer_mobile/need/layer.css'); 
    dynamicLoadJs('./js/layer_mobile/layer.js'); 
})()
